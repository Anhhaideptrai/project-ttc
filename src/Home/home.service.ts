import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpClientModule } from '@angular/common/http';
import { User } from './../Classes/user';
import { Observable } from 'rxjs';

@Injectable()
export class HomeAPIService {
  URL: string ="http://localhost:3000/user";
  constructor(private httpClient: HttpClient) { }

  getUser():Observable<any>
  {
    return this.httpClient.get(this.URL);
  }
  
}
