import { Component, OnInit } from '@angular/core';
import {
  FormControl,
  FormGroupDirective,
  NgForm,
  Validators,
} from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material/core';

/** Error when invalid control is dirty, touched, or submitted. */
export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(
    control: FormControl | null,
    form: FormGroupDirective | NgForm | null
  ): boolean {
    const isSubmitted = form && form.submitted;
    return !!(
      control &&
      control.invalid &&
      (control.dirty || control.touched || isSubmitted)
    );
  }
}

@Component({
  selector: 'app-user-infor',
  templateUrl: './user-infor.component.html',
  styleUrls: ['./user-infor.component.css'],
})
export class UserInforComponent implements OnInit {

  constructor() { }

  ngOnInit(): void { }
  emailFormControl = new FormControl('', [
    Validators.required,
    Validators.email,
  ]);

  matcher = new MyErrorStateMatcher();

  show = true;

  informations = [
    {
      id: 555,
      name: 'Phạm Thị Quỳnh Trang',
      pass: '12345',
      phone: '035516XXXX',
      address: 'Thanh Xuân - Hà Nội',
      email: 'phamthiquynhtrang458444@gmail.com',
    },
  ];

  //btn-submit
  id = this.informations[0].id;
  name = this.informations[0].name;
  pass = this.informations[0].pass;
  phone = this.informations[0].phone;
  address = this.informations[0].address;
  email = this.informations[0].email;
  newInformations = [
    {
      id: this.informations[0].id,
      name: this.informations[0].name,
      pass: this.informations[0].pass,
      phone: this.informations[0].phone,
      address: this.informations[0].address,
      email: this.informations[0].email,
    }
  ]

  getValueInfo() {
    this.newInformations[0] = { id: this.id, name: this.name, phone: this.phone, address: this.address, email: this.email, pass: this.pass }
    this.informations[0] = this.newInformations[0];
    console.log(this.newInformations);
  }

  hidden = "none"

}
