import { Component, OnInit } from '@angular/core';
import {MatTableDataSource} from '@angular/material/table';

export interface PeriodicElement {
  test_id: string;
  name: string;
  position: number;
  result: string;
  date: string;
  level: string;
}

const ELEMENT_DATA: PeriodicElement[] = [
  {position: 1,test_id:"T01", name: 'Tester',result: '14/20', date: '1/1/2021', level:'easy'},
  {position: 2,test_id:"T02", name: 'Java',result: '16/30', date: '1/1/2021', level:'normal'},
  {position: 3,test_id:"T03", name: 'JavaScript',result: '19/30', date: '1/1/2021', level:'hard'},
  {position: 4,test_id:"T04", name: 'HR',result: '8/10', date: '1/1/2021', level:'easy'},
  {position: 5,test_id:"T05", name: '.Net',result: '25/35', date: '1/1/2021', level:'normal'},
  {position: 6,test_id:"T06", name: 'IOS',result: '20/20', date: '1/1/2021', level:'easy'},
];

@Component({
  selector: 'app-history',
  templateUrl: './history.component.html',
  styleUrls: ['./history.component.css']
})
export class HistoryComponent implements OnInit {

  displayedColumns: string[] = ['position','test_id', 'name', 'result', 'date', 'level' ];
  dataSource = new MatTableDataSource(ELEMENT_DATA);
  
  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
   
  constructor() { }

  ngOnInit(): void {
  }

}
