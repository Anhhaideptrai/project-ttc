import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-do-test',
  templateUrl: './do-test.component.html',
  styleUrls: ['./do-test.component.css']
})
export class DoTestComponent implements OnInit {

  question: object[]=[
  {
    id:1,
    name:"Question-1",
    solutionA:"Anweser A",
    solutionB:"Anweser B",
    solutionC:"Anweser C",
    solutionD:"Anweser D",
    Anweser: "Anweser A"
  },
  {
    id:2,
    name:"Question-2",
    solutionA:"Anweser E",
    solutionB:"Anweser F",
    solutionC:"Anweser G",
    solutionD:"Anweser H",
    Anweser: "Anweser F"
  },
  {
    id:3,
    name:"Question-3",
    solutionA:"Anweser I",
    solutionB:"Anweser K",
    solutionC:"Anweser L",
    solutionD:"Anweser M",
    Anweser: "Anweser L"
  },
  {
    id:4,
    name:"Question-4",
    solutionA:"Anweser Ads",
    solutionB:"Anweser Bds",
    solutionC:"Anweser Cds",
    solutionD:"Anweser Dds",
    Anweser: "Anweser Dds"
  }
]

constructor() {  }

ngOnInit(): void {
  
}
//bindtimetest
timedata="1800";
state = "none";
hidden = "";
i = 0;
Anweser: string;
arrAnweser: string[]=[];

id=0;
name="";
solutionA="";
solutionB="";
solutionC="";
solutionD="";
Aws ="";

data=[];
point:number=0;
NUMBER_OF_QUESTION = this.question.length

showQuestion = (index)=>{
  let quizz = this.question[index];

  for(let q in quizz){
    if(quizz.hasOwnProperty(q)){
      this.data.push(quizz[q])
    }
  }
  for(let i = 0 ; i<=this.data.length; i++){
    this.id = this.data[0]
    this.name = this.data[1]
    this.solutionA = this.data[2]
    this.solutionB = this.data[3]
    this.solutionC = this.data[4]
    this.solutionD = this.data[5]
    this.Aws = this.data[6]
  }
  console.log(this.data)
}
startQuizz_Fnc(){
    this.state = "flex";
    this.hidden = "none";
    this.showQuestion(this.i);
}

nextquestion_fnc(){  
  if(this.Anweser != undefined){
    this.arrAnweser.push(this.Anweser);
    
  }else{
    this.arrAnweser.push(null)
  }
  
  this.i=this.i+1;

  if(this.Anweser == this.data[6]){
    this.point = this.point+1
  }

  if(this.i === this.question.length)
  {
    this.i = this.question.length-1
    if(window.confirm("Submit Your Test")){
      this.state ="none";
      this.isShow= "flex";
    }
  }

  console.log(this.arrAnweser+" "+ this.i+" " + this.data[6] +""+this.point);
  this.data =[];
  this.showQuestion(this.i)  
  }
  isShow="none"
submitTest_fnc(){
    if(window.confirm("Are you sure?")){
      this.state ="none";
      this.isShow= "flex";
    }
  }

back_fnc(){
    this.state ="none";
    this.isShow= "none";
    this.hidden="";
  }
}
