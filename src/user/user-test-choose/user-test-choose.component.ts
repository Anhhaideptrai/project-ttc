import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-user-test-choose',
  templateUrl: './user-test-choose.component.html',
  styleUrls: ['./user-test-choose.component.css']
})
export class UserTestChooseComponent implements OnInit {

  listTest:Object=[
    {
      testcode: 1,
      field: "Java",
      time:20,
      numquestion: 20,
      level: "easy"
    },
  ]
  panelOpenState = false;

  constructor() {  }
  
  ngOnInit(): void {
  }

}
