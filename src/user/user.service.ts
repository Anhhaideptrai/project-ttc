import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { User } from './../Classes/user';

@Injectable()
export class UserAPIService {
  UserId: number;
  URL: string="http://localhost:3000/user";
  constructor(private httpClient: HttpClient) { }

  // getUserWithParamater(): Observable<any>
  // {
  //   let param = new HttpParams().set('id', "UserId");
  //   return this.httpClient.get(this.URL, {params:param});
  // }
}
