import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModules } from './../../routerModule/router/router.module';
import { MaterialModule } from './../../materialModule/material.module';
import { ReactiveFormsModule, FormsModule} from '@angular/forms';

import { AdminComponent } from './../admin.component';
import { ManageAccountsComponent } from './../../admin/accounts/manage-accounts/manage-accounts.component';
import { EditAccountComponent } from './../../admin/accounts/edit-account/edit-account.component';
import { DeleteAccountComponent } from './../../admin/accounts/delete-account/delete-account.component';
import { AddAccountComponent } from './../../admin/accounts/add-account/add-account.component';
import { ManageTestComponent } from './../../admin/test/manage-test/manage-test.component';
import { AddTestComponent } from './../../admin/test/add-test/add-test.component';
import { EditTestComponent } from './../../admin/test/edit-test/edit-test.component';
import { DeleteTestComponent } from './../../admin/test/delete-test/delete-test.component';

@NgModule({
  declarations: [ 
    AdminComponent, 
    ManageAccountsComponent, 
    EditAccountComponent, 
    DeleteAccountComponent,
    AddAccountComponent,
    ManageTestComponent,
    AddTestComponent,
    EditTestComponent,
    DeleteTestComponent
   ],
   
  imports: [
    CommonModule,
    RouterModules,
    MaterialModule,
    ReactiveFormsModule, 
    FormsModule
  ]
})
export class AdminModule { }
