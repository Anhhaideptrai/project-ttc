import { Component, OnInit } from '@angular/core';


@Component({
  selector: 'app-add-test',
  templateUrl: './add-test.component.html',
  styleUrls: ['./add-test.component.css']
})
export class AddTestComponent implements OnInit {

  isSelected = false;
  constructor() { }

  ngOnInit(): void {
  }
  
  questions= [
    {id: 1,questionInfor: "Ai Là Người Giàu Nhất Thế gioi?",solutionA: "Trần Đức Duy",solutionB: "Doland Trump",solutionC: "BillGate",solutionD: "JAck MA",correct: "A",deTai: 1,mucDo: "Intern"},
    {id: 2,questionInfor: "1 + 1 = ?",solutionA: "Trần Đức Duy",solutionB: "Doland Trump",solutionC: "BillGate",solutionD: "JAck MA",correct: "A",deTai: 2,mucDo: "Fresher"},
    {id: 3,questionInfor: "Ông ăn chả bà ăn ...?",solutionA: "Trần Đức Duy",solutionB: "Doland Trump",solutionC: "BillGate",solutionD: "JAck MA",correct: "A",deTai: 1,mucDo: "Intern"},
    {id: 4,questionInfor: "Chó sủa là chó không ...?",solutionA: "Trần Đức Duy",solutionB: "Doland Trump",solutionC: "BillGate",solutionD: "JAck MA",correct: "A",deTai: 3,mucDo: "Junior"},
    {id: 5,questionInfor: "Trứng khôn hơn ...?",solutionA: "Trần Đức Duy",solutionB: "Doland Trump",solutionC: "BillGate",solutionD: "JAck MA",correct: "A",deTai: 3,mucDo: "Senior"},
    {id: 6,questionInfor: "Maketting là gì...?",solutionA: "Trần Đức Duy",solutionB: "Doland Trump",solutionC: "BillGate",solutionD: "JAck MA",correct: "A",deTai: 3,mucDo: "Intern"},
    {
      id: 6,questionInfor: "Sale là gì...?",
      solutionA: "Trần Đức Duy",
      solutionB: "Doland Trump",
      solutionC: "BillGate",
      solutionD: "JAck MA",
      correct: "A",
      deTai: 4,
      mucDo: "Intern"},
  ];
  questionFilter = [];
  questionsSelect = [];
  deTais=[
    {id:1,name:"developer"},
    {id:2,name:"tester"},
    {id:3,name:"maketing"},
    {id:3,name:"sale"},
  ];
  
  fillterPlant = 0;
  fillterLevel = "Select";
  
  filter(){
    if(this.fillterLevel == "Select" ){
      this.questionFilter = this.questions
    }
    else if(this.fillterLevel == "Intern" ){
       this.questionFilter = this.questions.filter(res => res.mucDo == "Intern")
        console.log(this.questions);
    }
     else if(this.fillterLevel == "Fresher"){
      this.questionFilter = this.questions.filter(res => res.mucDo == "Fresher")
      console.log(this.questions)
    }
    else if(this.fillterLevel == "Junior"){
      this.questionFilter = this.questions.filter(res => res.mucDo == "Junior")
      console.log(this.questions)
    }
    else if(this.fillterLevel == "Senior"){
      this.questionFilter = this.questions.filter(res => res.mucDo == "Senior")
      console.log(this.questions)
    }
  }
  filter1(){
    if(this.fillterPlant == 1 &&this.fillterLevel == "Intern"){
      this.questionFilter = this.questions.filter(res => res.deTai == 1&& res.mucDo=="Intern")
       console.log(this.questions);
   }
   else if(this.fillterPlant == 2&&this.fillterLevel == "Intern"){
    this.questionFilter = this.questions.filter(res => res.deTai == 2&& res.mucDo=="Intern")
    console.log(this.questions);
   }
   else if(this.fillterPlant == 3&&this.fillterLevel == "Intern"){
    this.questionFilter = this.questions.filter(res => res.deTai == 3&& res.mucDo=="Intern")
    console.log(this.questions);
   }
   else if(this.fillterPlant == 4&&this.fillterLevel == "Intern"){
    this.questionFilter = this.questions.filter(res => res.deTai == 4&& res.mucDo=="Intern")
    console.log(this.questions);
   }
    else if(this.fillterPlant == 0&&this.fillterLevel == "Intern"){
    this.questionFilter =  this.questions.filter(res => res.mucDo=="Intern")
    }
    else if(this.fillterPlant == 1&&this.fillterLevel == "Junior"){
      this.questionFilter = this.questions.filter(res => res.deTai == 1&& res.mucDo=="Junior")
      console.log(this.questions);
     }
     else if(this.fillterPlant == 2&&this.fillterLevel == "Junior"){
      this.questionFilter = this.questions.filter(res => res.deTai == 2&& res.mucDo=="Junior")
      console.log(this.questions);
     }
     else if(this.fillterPlant == 3&&this.fillterLevel == "Junior"){
      this.questionFilter = this.questions.filter(res => res.deTai == 3&& res.mucDo=="Junior")
      console.log(this.questions);
     }
     else if(this.fillterPlant == 4&&this.fillterLevel == "Junior"){
      this.questionFilter = this.questions.filter(res => res.deTai == 4&& res.mucDo=="Junior")
      console.log(this.questions);
     }
     else if(this.fillterPlant == 0&&this.fillterLevel == "Junior"){
      this.questionFilter =  this.questions.filter(res => res.mucDo=="Junior")
      }
     else if(this.fillterPlant == 1&&this.fillterLevel == "Fresher"){
      this.questionFilter = this.questions.filter(res => res.deTai == 1&& res.mucDo=="Fresher")
      console.log(this.questions);
     }
     else if(this.fillterPlant == 2&&this.fillterLevel == "Fresher"){
      this.questionFilter = this.questions.filter(res => res.deTai == 2&& res.mucDo=="Fresher")
      console.log(this.questions);
     }
     else if(this.fillterPlant == 3&&this.fillterLevel == "Fresher"){
      this.questionFilter = this.questions.filter(res => res.deTai == 3&& res.mucDo=="Fresher")
      console.log(this.questions);
     }
     else if(this.fillterPlant == 4&&this.fillterLevel == "Fresher"){
      this.questionFilter = this.questions.filter(res => res.deTai == 4&& res.mucDo=="Fresher")
      console.log(this.questions);
     }
     else if(this.fillterPlant == 0&&this.fillterLevel == "Fresher"){
      this.questionFilter =  this.questions.filter(res => res.mucDo=="Fresher")
      }
      else if(this.fillterPlant == 1&&this.fillterLevel == "Senior"){
        this.questionFilter = this.questions.filter(res => res.deTai == 1&& res.mucDo=="Senior")
        console.log(this.questions);
       }
       else if(this.fillterPlant == 2&&this.fillterLevel == "Senior"){
        this.questionFilter = this.questions.filter(res => res.deTai == 2&& res.mucDo=="Senior")
        console.log(this.questions);
       }
       else if(this.fillterPlant == 3&&this.fillterLevel == "Senior"){
        this.questionFilter = this.questions.filter(res => res.deTai == 3&& res.mucDo=="Senior")
        console.log(this.questions);
       }
       else if(this.fillterPlant == 4&&this.fillterLevel == "Senior"){
        this.questionFilter = this.questions.filter(res => res.deTai == 4&& res.mucDo=="Senior")
        console.log(this.questions);
       }
       else if(this.fillterPlant == 0&&this.fillterLevel == "Senior"){
        this.questionFilter =  this.questions.filter(res => res.mucDo=="Senior")
        }
     
   
  
  }
  oneToRight(){
    var select = (document.getElementById('list1') as HTMLSelectElement).value  
    if(select != ""){
      for(var i=0;i<this.questionFilter.length;i++){
        if(this.questionFilter[i].questionInfor.trim() == select){ 
          this.questionsSelect.push(this.questionFilter[i])
          console.log(this.questionFilter[i])
          var del = this.questionFilter.indexOf(this.questionFilter[i])
          this.questionFilter.splice(del,1)         
        }         
      }
    }
  }
  oneToLeft(){
    var select = (document.getElementById('list2') as HTMLSelectElement).value  
    if(select != ""){
      for(var i=0;i<this.questionsSelect.length;i++){
        if(this.questionsSelect[i].questionInfor.trim() == select){
          this.questionFilter.push(this.questionsSelect[i])
          console.log(this.questionsSelect[i])
          var del = this.questionsSelect.indexOf(this.questionsSelect[i])
          this.questionsSelect.splice(del,1) 
        }
      }
    }
  }
  allToRight(){
    for(var i=0;i<this.questionFilter.length;i++){
        this.questionsSelect.push(this.questionFilter[i])
        console.log(this.questionFilter[i])
        var del = this.questionFilter.indexOf(this.questionFilter[i])
        this.questionFilter.splice(del,1)         
      i--       
    }
  }
  allToLeft(){
    for(var i=0;i<this.questionsSelect.length;i++){
        this.questionFilter.push(this.questionsSelect[i])
        var del = this.questionsSelect.indexOf(this.questionsSelect[i])
        this.questionsSelect.splice(del,1)         
      i--       
    }
  }


}
