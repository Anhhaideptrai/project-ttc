
import { AfterViewInit,Component, OnInit ,ViewChild} from '@angular/core';
import {MatPaginator} from '@angular/material/paginator';
import {MatTableDataSource} from '@angular/material/table';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map, shareReplay } from 'rxjs/operators';
import {Router} from '@angular/router'

@Component({
  selector: 'app-manage-accounts',
  templateUrl: './manage-accounts.component.html',
  styleUrls: ['./manage-accounts.component.css']
})

export class ManageAccountsComponent implements OnInit {

  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
  .pipe(
    map(result => result.matches),
    shareReplay()
  );

  infor="Log Out";
  
  accounts = [
    {id: 1, name:'Trần Đức Duy',phoneNumber: '0336376262',address: 'Nam Định',email: 'tranducduy6262@gmail.com'},
    {id: 2, name:'Bùi Văn Nguyện',phoneNumber: '0847908199',address: 'Thanh Hóa',email: 'tranducduy6262@gmail.com'},
    {id: 3, name:'Vũ Quang Minh',phoneNumber: '0942422726',address: 'Hải Dương',email: 'tranducduy6262@gmail.com'},
    {id: 4, name:'Vũ Chí Công',phoneNumber: '09873232',address: 'Vĩnh Phúc',email: 'tranducduy6262@gmail.com'},
    {id: 5, name:'Hồ  Văn Nam',phoneNumber: '09687662626',address: 'Nghệ An',email: 'tranducduy6262@gmail.com'},
  ];
 
  constructor(private breakpointObserver: BreakpointObserver) {}
  ngOnInit(): void {
    
  }
  
  

}




