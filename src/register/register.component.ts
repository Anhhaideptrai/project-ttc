import { Component, OnInit } from '@angular/core';
import {FormControl, Validators} from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})

export class RegisterComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit(): void {
  }

  //Regexception
  userFormControl = new FormControl('',[Validators.required ])
  emailFormControl = new FormControl('', [
    Validators.required,
    Validators.email,
  ]);
  passwordFormControl = new FormControl('',[Validators.required])
  repasswordFormControl = new FormControl('',[Validators.required])

  userName : string = "";
  password : string = "";
  repassword : string = "";
  email : string = "#";
  USER ={
    user: "",
    pass :  "",
    email : ""
  }

  Register_Fnc(){    
   if(this.repassword == this.password && this.password !== ""){
     this.USER.user = this.userName;
     this.USER.pass = this.password;
     this.USER.email = this.email;
     if(window.confirm("Register Sucessfully, Login Now?"))   
     {
      this.router.navigate([''])  
     }
   }else{
     alert("Something Wrong !!!")
     this.router.navigate(['/register'])
   }
    
  }
}
