export class User{
    id: number;
    user: string;
    password: string;
    email: string;
    phone: string;
    address: string;
    role: boolean;

    constructor( id, user, password, email, phone, address, role)
    {
        this.id = id;
        this.password = password;
        this.email = email;
        this.phone = phone;
        this.address = address;
        this.role = role;
        this.user = user;
        
    }
}







