import { NgModule } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatSelectModule } from '@angular/material/select';
import { MatRadioModule } from '@angular/material/radio';
import { MatCardModule } from '@angular/material/card';
import { MatInputModule } from '@angular/material/input';
import {MatSliderModule} from '@angular/material/slider';
import {MatProgressBarModule} from '@angular/material/progress-bar';
import {MatTableModule} from '@angular/material/table';
import {MatExpansionModule} from '@angular/material/expansion';
import { LayoutModule } from '@angular/cdk/layout';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import {MatDialogModule} from '@angular/material/dialog';
import {MatMenuModule} from '@angular/material/menu';
import {MatGridListModule} from '@angular/material/grid-list';
import {MatBadgeModule} from '@angular/material/badge';
import {MatDatepickerModule, } from '@angular/material/datepicker';
import {MatNativeDateModule} from '@angular/material/core';

const MaterialComponent=[  
    MatButtonModule, 
    MatSelectModule, 
    MatRadioModule, 
    MatCardModule, 
    MatSliderModule, 
    MatInputModule,
    LayoutModule ,
    MatToolbarModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    MatDialogModule,
    MatProgressBarModule,
    MatMenuModule,
    MatGridListModule,
    MatBadgeModule,
    MatTableModule,
    MatExpansionModule,
    MatDatepickerModule,
    MatNativeDateModule
  ]

@NgModule({
  imports: [
    MatButtonModule, 
    MatSelectModule, 
    MatRadioModule, 
    MatCardModule, 
    MatSliderModule, 
    MatInputModule,
    LayoutModule ,
    MatToolbarModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    MatDialogModule,
    MatProgressBarModule,
    MatMenuModule,
    MatGridListModule,
    MatBadgeModule,
    MatTableModule ,
    MatExpansionModule,
    MatDatepickerModule,
    MatNativeDateModule 
  ],
  exports: [
    MatButtonModule, 
    MatSelectModule, 
    MatRadioModule, 
    MatCardModule, 
    MatSliderModule, 
    MatInputModule,
    LayoutModule ,
    MatToolbarModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    MatDialogModule,
    MatProgressBarModule,
    MatMenuModule,
    MatGridListModule,
    MatBadgeModule,
    MatTableModule,
    MatExpansionModule,
    MatDatepickerModule,
    MatNativeDateModule
  ]
})
export class MaterialModule { }